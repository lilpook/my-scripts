import sys, binascii, re, mmap, timeit
start = timeit.default_timer()
request = re.compile(b'\x00\x00\x00')

filename = sys.argv[1]

with open(filename, 'rb') as f:
#    content = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
    for match_obj in request.finditer(f.read()):
        offset = match_obj.start()
        print("decimal: {} ".format(offset))
        print ("hex(): " + hex(offset))
        print ('formatted hex: {:02X} \n'.format(offset))
end = timeit.default_timer()
print (end - start)

#offsets = re.search(request,f.read())

#print('found a match:'), offsets .group()
#hex_string = binascii.hexlify(content)
#print re.findall(request, hex_string)
