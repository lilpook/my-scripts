iptables -t nat -A PREROUTING -i tor0 -d 192.168.0.0/24 -j RETURN
iptables -t nat -A PREROUTING -i tor0 -p udp -m udp --dport 53 -j REDIRECT --to-port 9053
iptables -t nat -A PREROUTING -i tor0 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j REDIRECT --to-port 9040
iptables -t nat -I POSTROUTING -s 172.16.0.0/24 -j MASQUERADE
