sudo ip addr add 139.96.30.100/24 dev wlp0s19f1u2
iptables -t nat -A POSTROUTING -o enp2s0 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0_ap -o enp4s0 -j ACCEPT
