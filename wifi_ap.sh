#!/usr/bin/bash
iw dev wlp0s19f2u3 interface add wlan0_sta type station
iw dev wlp0s19f2u3 interface add wlan0_ap  type __ap
ip link set dev wlan0_sta address 12:34:56:78:ab:cd
ip link set dev wlan0_ap  address 12:34:56:78:ab:ce
#ip addr add 139.96.30.100/24 dev wlan0_ap
iptables -t nat -A POSTROUTING -o enp4s0 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0_ap -o enp4s0 -j ACCEPT
