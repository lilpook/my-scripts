import sys 
import string 
f = open(sys.argv[1],'r').read().upper()
f = "".join(f.split())
f_length = len(f)
print("length is" + str(f_length))
alphabet = dict.fromkeys(string.ascii_uppercase, 0)
for char in f:
    try:
        alphabet[char] += 1
    except:
        print("wrong chars in file (not letters)")

for i in alphabet:
    percent = round(( alphabet[i]/(f_length/100) ),2)
    print(i + " : " + repr(alphabet[i]) + " ( " + repr(percent) + "% )") 
