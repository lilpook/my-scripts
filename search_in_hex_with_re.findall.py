import sys, binascii, re, mmap

request = sys.argv[2]
filename = sys.argv[1]

with open(filename, 'rb') as f:
    content = f.read()
#    print(content.find(request))
hex_string = binascii.hexlify(content)

print(re.match(request, hex_string))
