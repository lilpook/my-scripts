#!/usr/bin/env bash 

#считает количество строк с фразой "authorization error" в неком файле и выводит это количество с разбивкой по дням. 

while getopts f:d: option
do
	case "${option}"
		in
		f) FILES=${OPTARG};; #should get multiply files
		d) DAYS=${OPTARG};;
		?) printf "Usage: %s: [-f] files [-d] number of last days for searching"
	esac
done

DATE=$(date -I -d "- $DAYS day")
LOGS=/tmp/err_count.tmp

grep -i 'error' $FILES >> $LOGS

while [ "$DATE" != `date -I -d '+ 1 day'` ]; do
	cnt=$(grep $DATE $LOGS | wc -l)
	printf '%d %s\n' "$cnt" "$DATE"
	DATE=$(date -I -d "$DATE + 1 day")
done
rm $LOGS
	



